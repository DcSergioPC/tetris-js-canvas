const canva = document.getElementById('canvas');
const ctx = canva.getContext('2d');

const audio = new Audio('tetris.mp3')
const audio2 = new Audio('gameover.mp3')
audio.addEventListener('ended', function () {
  
  // Reiniciar la reproducción desde el principio
  audio.currentTime = 0;
  audio.play()
})

let score = 0
const WINDOW_HEIGHT = window.innerHeight/30;
const BLOCK_SIZE = WINDOW_HEIGHT;
const BLOCK_WIDTH = 15;
const BLOCK_HEIGHT = 30;
let TABLE = Array.from({ length: BLOCK_HEIGHT }, () => Array(BLOCK_WIDTH).fill(0));
canva.width = BLOCK_WIDTH * BLOCK_SIZE;
canva.height = BLOCK_HEIGHT * BLOCK_SIZE;
ctx.scale(BLOCK_SIZE, BLOCK_SIZE);

ctx.fillStyle = 'black';
ctx.fillRect(0, 0, BLOCK_WIDTH, BLOCK_HEIGHT);

ctx.font = '5px Arial';
ctx.fillStyle = 'white';

const texto = `Start`;
ctx.strokeStyle = 'blue';
ctx.lineWidth = 0.3;
ctx.strokeText(texto, 2, BLOCK_HEIGHT/2);
ctx.fillText(texto, 2, BLOCK_HEIGHT/2);

const colors = [
  'red',
  'blue',
  'green',
  'pink',
  'yellow',
  'purple',
  'brown',
  'orange',
  'white'
]

let color = colors[Math.floor(Math.random()*colors.length)]

let countColors = 0
function setRandomColors(){
  color = colors[countColors%colors.length]
  countColors++;
}
setRandomColors()

const PIECES = [
  [
    [0,1,0],
    [1,1,1]
  ],
  [
    [1,1],
    [1,1]
  ],
  [
    [1,1,1,1]
  ],
  [
    [1,1,1],
    [0,0,1]
  ],
  [
    [0,0,1],
    [1,1,1]
  ],
  [
    [1,1,0],
    [0,1,1]
  ],
  [
    [0,1,1],
    [1,1,0]
  ]
]

let dropCounter = 0
let lastTime = 0

function changePieza(){
  const indice = Math.floor(Math.random()*PIECES.length)
  pieza.shape = PIECES[indice]
  pieza.position = {x: Math.floor(Math.random()*(BLOCK_WIDTH-pieza.shape[0].length+1)), y: 0}
}
const pieza = {
  shape: PIECES[0],
  position: {x: 0, y: 0}
}

changePieza();

let prev = {...pieza.position};


function start(event){
  update()
  audio2.pause()
  audio2.currentTime = 0;
  audio.play()
  document.removeEventListener("click",start)
  document.removeEventListener("keydown",start)
}

document.addEventListener("click", start)
document.addEventListener("keydown", start)

function gameOver(){
  audio2.play()
  ctx.font = '5px Arial';
  ctx.fillStyle = 'white';
  ctx.strokeStyle = '#123456';
  ctx.lineWidth = 0.5;

  ctx.strokeText("Game", 0.7, BLOCK_HEIGHT/2-2);
  ctx.strokeText("Over", 2, BLOCK_HEIGHT/2+2);
  ctx.fillText("Game", 0.7, BLOCK_HEIGHT/2-2);
  ctx.fillText("Over", 2, BLOCK_HEIGHT/2+2);
  ctx.font = '0.5px Arial';
  ctx.lineWidth = 0.3;
  ctx.strokeText(`Score: ${score}`, BLOCK_WIDTH/2-1, BLOCK_HEIGHT/2+3);
  ctx.fillText(`Score: ${score}`, BLOCK_WIDTH/2-1, BLOCK_HEIGHT/2+3);
  document.addEventListener("click", start)
  document.addEventListener("keydown", start)
  score = 0
  audio.pause()
  audio.currentTime = 0;
}

function update(time = 0){
  const deltaTime = time - lastTime
  lastTime = time
  dropCounter += deltaTime

  prev = {...pieza.position}
  if(dropCounter>1000){
    pieza.position.y++
    dropCounter=0
  }
  
  const limit = checkBounds();
  if(limit !== 0) pieza.position = prev;
  if(limit === 1) save();
  if(checkBounds()!==0){
    // alert("Game Over")
    TABLE = TABLE.map((rows,y)=>{
      return rows.map(()=> 0)
    })
    gameOver()
  }else{
    draw();
    window.requestAnimationFrame(update);
  }
}

function draw(){

  TABLE.forEach((rows, y) => {
    if(rows.every(e=>e===1)){
      TABLE.splice(y,1);
      TABLE = [new Array(BLOCK_WIDTH).fill(0), ...TABLE];
      score+=BLOCK_WIDTH
    }
  });

  TABLE.forEach((rows, y) => {
    rows.forEach((value, x)=>{
      if(value===0){
        ctx.fillStyle = "black"
        ctx.fillRect(x, y, 1, 1);
      }
    })
  });

  pieza.shape.forEach((rows, y) => {
    rows.forEach((value, x)=>{
      if(value!==0) {
        ctx.fillStyle = color
        ctx.fillRect(x + pieza.position.x, y + pieza.position.y, 1, 1);
      }
    })
  });
  for(let i=0; i<=BLOCK_HEIGHT;i++){
    ctx.beginPath();
    ctx.strokeStyle = "gray";
    ctx.lineWidth = 0.02;
    ctx.moveTo(0, i);
    ctx.lineTo(BLOCK_WIDTH, i);
    ctx.stroke()
  }
  for(let i=0; i<=BLOCK_WIDTH;i++){
    ctx.beginPath(); 
    ctx.strokeStyle = "gray";
    ctx.lineWidth = 0.02;
    ctx.moveTo(i, 0); 
    ctx.lineTo(i, BLOCK_HEIGHT); 
    ctx.stroke()
  }
  ctx.font = '0.5px Arial';
  ctx.fillStyle = 'white';

  // Dibujar texto en el canvas
  const texto = `Score: ${score}`;
  ctx.fillText(texto, 0, 0.5);
}

function checkBounds(){
  let colision = 0
  pieza.shape.forEach((rows,y)=>{
    rows.forEach((value,x)=>{
      if(TABLE[pieza.position.y + y]?.[pieza.position.x + x] === undefined && pieza.position.y + y < BLOCK_HEIGHT)
        colision = -1
      if(pieza.position.y + y >= BLOCK_HEIGHT || (TABLE[pieza.position.y + y][pieza.position.x + x] === 1 && pieza.shape[y][x]===1))
        colision = 1
    })
  })
  return colision
}

function save(){
  pieza.shape.forEach((rows,y)=>{
    rows.forEach((value,x)=>{
      if(pieza.shape[y][x]===1)
        TABLE[pieza.position.y + y][pieza.position.x + x]=1
    })
  })
  draw()
  changePieza();
  setRandomColors();
}

function rotate() {
  let shape = [];
  for(let i=pieza.shape[0].length-1;i>=0;i--){
    let a = [];
    for(let j=0;j<pieza.shape.length;j++){
      a.push(pieza.shape[j][i]);
    }
    shape.push(a);
  }
  while(shape[0].length+pieza.position.x>BLOCK_WIDTH)
    pieza.position.x--;

  let colision = true
  shape.forEach((rows,y)=>{
    rows.forEach((value,x)=>{
      if(TABLE[pieza.position.y + y][pieza.position.x + x] === 1 && pieza.shape[y][x]===1)
        colision = false
    })
  })
  if(colision) 
    pieza.shape = shape;
}

document.addEventListener("keydown",event=>{
  prev = {...pieza.position}
  if(event.key === 'ArrowRight') pieza.position.x++;
  else if(event.key === 'ArrowLeft') pieza.position.x--;
  else if(event.key === 'ArrowDown') pieza.position.y++;
  else if(event.key === 'ArrowUp') rotate();
  const limit = checkBounds();
  if(limit !== 0) pieza.position = prev;
  if(limit === 1) save();
})



